<?php

/**
 * Created by PhpStorm.
 * User: josh
 * Date: 7/8/16
 * Time: 7:24 PM
 */
class CF_Braintree_Single {

	

	/**
	 * Proccess BrainTree Single Payment
	 *
	 * @since 1.3.0
	 *
	 * @param array $config Processor config
	 * @param array $form Form config
	 * @param string $proccesid Unique ID for this instance of the processor
	 * @param object|Caldera_Forms_Processor_Get_Data $data_object Data object
	 *
	 * @return array
	 */
	public static function do_payment( array $config, array $form, $proccesid, Caldera_Forms_Processor_Get_Data $data_object ) {
		global $transdata;
		// check for errors before we continue to processing
		if (  empty( $transdata[ $proccesid ] ) ) {
			$transdata[ $proccesid ] = array();

			$purchase_data = array();

			$order_id = $data_object->get_value( 'orderId' );
			if ( empty( $order_id ) ) {
				$key      = '_cf_braintree_last_order_id_' . $form[ 'ID' ];
				$order_id = get_option( $key, 0 );
				$order_id ++;
				update_option( $key, $order_id );
			}

			$submit_for_settlement = $data_object->get_value( 'submitForSettlement' );
            if ( empty( $submit_for_settlement ) || 'on' === $submit_for_settlement ) {
                $submit_for_settlement = true;
			}else{
			    $submit_for_settlement = false;
            }

			$transaction = array(
				'orderId'           => $order_id,
				'amount'            => $data_object->get_value( 'amount' ),
				'merchantAccountId' => $data_object->get_value( 'braintree_merchant_account_id' ),
				'creditCard'        => array(
					'cardholderName'  => $data_object->get_value( 'cardholderName' ),
					'number'          => $data_object->get_value( 'card_number' ),
					'expirationMonth' => $data_object->get_value( 'card_exp_month' ),
					'expirationYear'  => $data_object->get_value( 'card_exp_year' ),
					'cvv'             => $data_object->get_value( 'card_cvc' ),
				),
				'customer'          => array(
					'firstName' => $data_object->get_value( 'first_name' ),
					'lastName'  => $data_object->get_value( 'last_name' ),
					'email'     => $data_object->get_value( 'customer_email' ),
				),
				'billing'           => array(
					'streetAddress'     => $data_object->get_value( 'card_address' ),
					'extendedAddress'   => $data_object->get_value( 'card_address_2' ),
					'locality'          => $data_object->get_value( 'card_city' ),
					'region'            => $data_object->get_value( 'card_state' ),
					'postalCode'        => $data_object->get_value( 'card_zip' ),
					'countryCodeAlpha2' => $data_object->get_value( 'card_country' ),
				),
				'options'           => array(
					'submitForSettlement' => $submit_for_settlement
				)
			);


			/**
			 * Runs before payment is attempted via Braintree.
			 *
			 * @since 1.2.0
			 *
			 * @param array $transaction Data to send to Braintree
			 * @param array $config Processor config
			 * @param array $form Form config
			 * @param string $proccesid Unique ID for this instance of the processor
			 */
			$transaction = apply_filters( 'cf_braintree_transaction_data', $transaction, $config, $form, $proccesid );

			if ( $data_object->get_value( 'sandbox' ) ) {
				Braintree_Configuration::environment( 'sandbox' );
			} else {
				Braintree_Configuration::environment( 'production' );
			}

			Braintree_Configuration::merchantId( $data_object->get_value( 'braintree_merchant_id' ) );
			Braintree_Configuration::publicKey( $data_object->get_value( 'braintree_public_key' ) );
			Braintree_Configuration::privateKey( $data_object->get_value( 'braintree_private_key' ) );

			$result = Braintree_Transaction::sale( $transaction );

			if ( $result->success ) {
				$transdata[ $proccesid ][ 'meta' ] = cf_braintree_prepare_meta( $result );

				/**
				 * Runs after a successful BrainTree payment is made
				 *
				 * @since 1.2.0
				 *
				 * @param object|\Braintree_Transaction $result Transaction result object.
				 * @param array $transaction Data sent to Braintree.
				 * @param string $order_id Order ID.
				 * @param array $config Processor config
				 * @param array $form Form config
				 * @param string $proccesid Unique ID for this instance of the processor
				 */
				do_action( 'cf_braintree_success', $result, $order_id, $transaction, $config, $form, $proccesid );

			} else {

				$message = $result->__get( 'message' );
				if ( strpos( $message, '\n' ) ) {
					$messages = explode( "\n", $message );
					foreach ( $messages as $message ) {
						$data_object->add_error( $message );
					}
				} else {
					$data_object->add_error( $message );
				}


			}

		} else {
			$data_object->add_error( __( 'Transaction Failed', 'cf-braintree' ) );

		}

		return $data_object;

	}
}