<?php


class CF_Braintree_Base extends Caldera_Forms_Processor_Payment implements Caldera_Forms_Processor_Interface_Payment {

	/**
	 * Run the BrainTree if possible, and if not return errors.
	 *
	 * @since 1.2.0
	 *
	 * @param array $config Processor config
	 * @param array $form Form config
	 * @param string $proccesid Unique ID for this instance of the processor
	 *
	 * @return array
	 */
	public function pre_processor( array $config, array $form, $proccesid ) {

		if( 'charge' == $config[ 'cf-braintree-planOrSingle' ] ){
			$_fields = wp_list_pluck( $this->fields, 'id' );
			foreach ( array( 'cf-braintree-plan-select', 'cf-braintree-plan-actual'  ) as $_field ){
				unset( $config[ $_field ] );
				$index = array_search( $_field, $_fields );
				unset( $this->fields[ $index ] );
			}
			unset( $config[ 'cf-braintree-plan-select' ] );
			unset( $config[ 'cf-braintree-plan-actual' ] );
		}

		$this->set_data_object_initial( $config, $form );

		$values = $this->data_object->get_values();

		if ( ! $values[ 'sandbox' ] && ! is_ssl() ) {
			$this->data_object->add_error( __( 'Payment was not be processed over HTTP.', 'cf-braintree' ) );
		}

		$errors = $this->data_object->get_errors();
		if ( ! empty( $errors ) ) {
			return $errors;
		}


		$this->data_object = $this->do_payment( $config, $form, $proccesid, $this->data_object );
		$this->setup_transata( $proccesid );
		$errors                              = $this->data_object->get_errors();
		if ( ! empty( $errors ) ) {
			return $errors;

		}
	}

	/**
	 * If we made BrainTree sale, hash credit card before saving.
	 *
	 * @since 1.2.0
	 *
	 * @param array $config Processor config
	 * @param array $form Form config
	 * @param string $proccesid Process ID
	 *
	 * @return array
	 */
	public function processor( array $config, array $form, $proccesid ) {
		$this->setup_transata( $proccesid );
		if( ! is_object( $this->data_object )) {

		}


		$data           = $this->data_object->get_values();
		$fields         = $this->data_object->get_fields();

		if ( isset( $config[ 'card_number' ] ) && ! empty( $config[ 'card_number' ] ) ) {

			$number = $data[ 'card_number' ];
			$number = $number = substr( $number, 0, 4 ) . str_repeat( 'X', strlen( $number ) - 4 );
			$field  = $fields[ 'card_number' ][ 'config_field' ];
			if ( $field ) {
				Caldera_Forms::set_field_data( $field, $number, $form );
			}
		}

		if ( isset( $config[ 'card_cvc' ] ) && ! empty( $config[ 'card_cvc' ] ) ) {
			$number = str_repeat( 'X', strlen( $data[ 'card_cvc' ] ) );
			$field  = $fields[ 'card_cvc' ][ 'config_field' ];
			if ( $field ) {
				Caldera_Forms::set_field_data( $field, $number, $form );
			}
		}

		if ( isset( $config[ 'card_exp_month' ] ) && ! empty( $config[ 'card_exp_month' ] ) ) {
			$number =  'xx';
			$field  = $fields[ 'card_exp_month' ][ 'config_field' ];
			if ( $field ) {
				Caldera_Forms::set_field_data( $field, $number, $form );
			}
		}

		if ( isset( $config[ 'card_exp_year' ] ) && ! empty( $config[ 'card_exp_year' ] ) ) {
			$number =  'xx';
			$field  = $fields[ 'card_exp_year' ][ 'config_field' ];
			if ( $field ) {
				Caldera_Forms::set_field_data( $field, $number, $form );
			}
		}

		if ( ! isset( $transdata[ $proccesid ][ 'meta' ] ) ) {
			$transdata[ $proccesid ][ 'meta' ] = array();
		}

		return $transdata[ $proccesid ][ 'meta' ];

	}


	/**
	 * Proccess BrainTree Payment
	 *
	 * @since 1.2.0
	 *
	 * @param array $config Processor config
	 * @param array $form Form config
	 * @param string $proccesid Unique ID for this instance of the processor
	 * @param object|Caldera_Forms_Processor_Get_Data $data_object Data object
	 *
	 * @return array
	 */
	public function do_payment( array $config, array $form, $proccesid, Caldera_Forms_Processor_Get_Data $data_object ) {
		if( 'plan' == $data_object->get_value( 'cf-braintree-planOrSingle' ) ){
			return CF_Braintree_Plan::do_payment( $config, $form, $proccesid, $data_object );
		}else{
			return CF_Braintree_Single::do_payment( $config, $form, $proccesid, $data_object );
		}
	}
	




	
	


}
