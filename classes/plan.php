<?php

/**
 * Created by PhpStorm.
 * User: josh
 * Date: 7/8/16
 * Time: 7:31 PM
 */
class CF_Braintree_Plan   {

	/**
	 * Proccess BrainTree Payment
	 *
	 * @since 1.2.0
	 *
	 * @param array $config Processor config
	 * @param array $form Form config
	 * @param string $proccesid Unique ID for this instance of the processor
	 * @param object|Caldera_Forms_Processor_Get_Data $data_object Data object
	 *
	 * @return Caldera_Forms_Processor_Get_Data
	 */
	public static function do_payment( array $config, array $form, $proccesid, Caldera_Forms_Processor_Get_Data $data_object ) {
		global $transdata;
		// check for errors before we continue to processing
		if (  empty( $transdata[ $proccesid ] ) ) {
			$transdata[ $proccesid ] = array();

			$customer = array(

				'firstName' => $data_object->get_value( 'first_name' ),
				'lastName'  => $data_object->get_value( 'last_name' ),
				'email'     => $data_object->get_value( 'customer_email' ),
				'creditCard' => array(
					'cardholderName'  => $data_object->get_value( 'cardholderName' ),
					'number'          => $data_object->get_value( 'card_number' ),
					'expirationMonth' => $data_object->get_value( 'card_exp_month' ),
					'expirationYear'  => $data_object->get_value( 'card_exp_year' ),
					'cvv'             => $data_object->get_value( 'card_cvc' ),
					'billingAddress' => array(
						'streetAddress'     => $data_object->get_value( 'card_address' ),
						'extendedAddress'   => $data_object->get_value( 'card_address_2' ),
						'locality'          => $data_object->get_value( 'card_city' ),
						'region'            => $data_object->get_value( 'card_state' ),
						'postalCode'        => $data_object->get_value( 'card_zip' ),
						'countryCodeAlpha2' => $data_object->get_value( 'card_country' ),
					)
				)

			);
			if ( $data_object->get_value( 'sandbox' ) ) {
				Braintree_Configuration::environment( 'sandbox' );
			} else {
				Braintree_Configuration::environment( 'production' );
			}


			Braintree_Configuration::merchantId( $data_object->get_value( 'braintree_merchant_id' ) );
			Braintree_Configuration::publicKey( $data_object->get_value( 'braintree_public_key' ) );
			Braintree_Configuration::privateKey( $data_object->get_value( 'braintree_private_key' ) );

			/**
			 * Runs before customer for subscription is created via Braintree.
			 *
			 * @since 1.3.0
			 *
			 * @param array $customer Data to send to Braintree
			 * @param array $config Processor config
			 * @param array $form Form config
			 * @param string $proccesid Unique ID for this instance of the processor
			 */
			$customer = apply_filters( 'cf_braintree_customer_data', $customer, $config, $form, $proccesid );
			try {
				$customer_result = Braintree_Customer::create( $customer );
				if( $customer_result->success ){

					/** @var Braintree_Customer $customer */
					$customer = $customer_result->customer;

					/**
					 * Runs after a customer for a BrainTree subscription is made, before they are added to subscription plan
					 *
					 * @since 1.3.0
					 *
					 * @param Braintree_Customer $customer
					 * @param array $customer Data to send to Braintree
					 * @param array $config Processor config
					 * @param array $form Form config
					 * @param string $proccesid Unique ID for this instance of the processor
					 */
					do_action( 'cf_braintree_customer_created', $customer_result, $customer, $config, $form, $proccesid );
					$methods = $customer->paymentMethods;
					if( ! empty( $methods ) ){

						foreach( $methods as $method ){
							if( is_a( $method, 'Braintree_CreditCard' ) ){
								$token = $method->token;
								try{
									$result = Braintree_Subscription::create(array(
										'paymentMethodToken' => $token,
										'planId' => $data_object->get_value( 'cf-braintree-plan-actual' )
									));

									if( $result->success ) {
										/**
										 * Runs after customer is added to subscription plan
										 *
										 * @since 1.3.0
										 *
										 * @param object|\Braintree_Transaction $result Transaction result object.
										 * @param Braintree_Customer $customer
										 * @param array $customer Data to send to Braintree
										 * @param array $config Processor config
										 * @param array $form Form config
										 * @param string $proccesid Unique ID for this instance of the processor
										 */
										do_action( 'cf_braintree_subscription_created', $result, $customer_result, $customer, $config, $form, $proccesid );
										$transdata[ $proccesid ][ 'meta' ] = cf_braintree_prepare_meta( $result );
										return $data_object;
									}

								}catch ( Exception $e){
									$data_object->add_error( $e->getMessage() );
									return $data_object;
								}
							}
						}
					}

				}
			}catch (Exception $e){
				$data_object->add_error( $e->getMessage() );
				return $data_object;
			}



		} else {
			$data_object->add_error( __( 'Transaction Failed', 'cf-braintree' ) );

		}

		return $data_object;

	}
	

}