<?php
/**
 * Plugin Name: BrainTree for Caldera Forms
 * Plugin URI:  https://CalderaWP.com/downloads/braintree-for-caldera-forms
 * Description: BrainTree for Caldera Forms
 * Version: 1.2.4
 * Author:      Caldera Labs
 * Author URI:  https://Calderaforms.com
 * License:     GPLv2+
 * Text Domain: cf-braintree
 * Domain Path: /languages
 */

/**
 * Copyright (c) 2015 Josh Pollock for CalderaWP LLC (email : Josh@CalderaWP.com) for CalderaWP LLC
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2 or, at
 * your discretion, any later version, as published by the Free
 * Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/**
 * Define constants
 */
define( 'CF_BRAINTREE_VER', '1.2.4' );
define( 'CF_BRAINTREE_URL',     plugin_dir_url( __FILE__ ) );
define( 'CF_BRAINTREE_PATH',    dirname( __FILE__ ) . '/' );
define( 'CF_BRAINTREE_CORE',    dirname( __FILE__ )  );

/**
 * Default initialization for the plugin:
 * - Registers the default textdomain.
 */
function cf_braintree_init_text_domain() {
	load_plugin_textdomain( 'cf-braintree', FALSE, CF_BRAINTREE_PATH . 'languages' );
}

// Load
add_action( 'plugins_loaded', 'cf_brain_tree_init' );
function cf_brain_tree_init(){

	/**
	 * Include Files
	 */

	if( cf_brain_tree_load_dependencies() ){
		// load dependencies
		include_once CF_BRAINTREE_PATH . 'vendor/autoload.php';

	}

	// pull in the functions file
	include CF_BRAINTREE_PATH . 'includes/functions.php';

	/**
	 * Hooks
	 */
	//register text domain
	add_action( 'init', 'cf_braintree_init_text_domain' );

	// filter to initialize the license system
	add_action( 'admin_init', 'cf_braintree_init_license' );

	//add our example form
	add_filter( 'caldera_forms_get_form_templates', 'cf_braintree_example_form' );

	//load up the processor
	add_action( 'caldera_forms_pre_load_processors', 'cf_braintree_init' );

	add_action( 'wp_ajax_cf_braintree_get_plans', 'cf_braintree_get_plans_ajax_cb' );

	//add refresh lists button to list input
	add_filter( 'caldera_forms_processor_ui_input_html', function( $field, $type, $id ){
		if ( 'cf-braintree-plan-select' == $id ) {
			$spinner = '<span id="cf-braintree-plans-spinner" class="spinner" aria-hidden="true" style="display: none;"></span>';
			$field .= sprintf( '<button class="button" id="cf-braintree-get-plans" title="%s" aria-describedby="cf-braintree-plan-refresh-description">%s</button>',
				esc_attr( 'Click to fetch list of plans from BrainTree', 'cf-braintree' ),
				esc_html__( 'Refresh Plans List', 'cf-braintree' ) . $spinner
			);

			$field .= sprintf( '<p class="description" id="cf-braintree-plan-refresh-description">%s</p>',
				esc_html__( 'Subscription plans must be created in your BrainTree dashboard', 'cf-braintree' )
			);
		}

		return $field;
	}, 10, 3 );


}

/**
 * Check if we should load dependencies
 *
 * @since 1.2.2
 *
 * @return bool
 */
function cf_brain_tree_load_dependencies(){
	/**
	 * Controls if dependencies should load
	 *
	 * @since 1.2.2
	 *
	 * @param bool $load If to load
	 */
	return (bool) apply_filters( 'cf_brain_tree_load_dependencies', true );
}