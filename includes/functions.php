<?php
/**
 * Functions for BrainTree for Caldera Forms
 *
 * @package   cf_braintree
 * @author    Josh Pollock for CalderaWP LLC (email : Josh@CalderaWP.com)
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 Josh Pollock for CalderaWP LLC
 */


/**
 * Registers the BrainTree for Caldera Forms Processor
 *
 * @uses "plugins_loaded" action
 *
 * @since 1.2.0

 * @return array	Array of registered processors
 */
function cf_braintree_init(){
	if ( ! class_exists( 'Caldera_Forms_Processor_Get_Data' ) || ! class_exists( 'Caldera_Forms_Processor_Processor' ) ) {
		return;

	}

	Caldera_Forms_Autoloader::add_root( 'CF_Braintree', CF_BRAINTREE_PATH. 'classes' );

	$processor = array(
		"name"				=>	__( 'BrainTree for Caldera Forms', 'cf-braintree'),
		"description"		=>	__( 'BrainTree for Caldera Forms', 'cf-braintree'),
		"icon"				=>	CF_BRAINTREE_URL . "icon.png",
		"author"			=>	'Caldera Labs',
		"author_url"		=>	'https://Calderaforms.com',

		"template"			=>	CF_BRAINTREE_PATH . "includes/config.php",

	);

	new CF_Braintree_Base( $processor, cf_braintree_fields(), 'cf_braintree' );
}




/**
 * Initializes the licensing system
 *
 * @uses "admin_init" action
 *
 * @since 0.1.0
 */
function cf_braintree_init_license(){

	if( class_exists( '\\calderawp\\licensing_helper\\licensing') ) {
		$plugin = array(
			'name'      => 'BrainTree for Caldera Forms',
			'slug'      => 'braintree-for-caldera-forms',
			'url'       => 'https://calderaforms.com/',
			'version'   => CF_BRAINTREE_VER,
			'key_store' => 'cf_braintree_license',
			'file'      => CF_BRAINTREE_CORE,
		);

		new \calderawp\licensing_helper\licensing( $plugin );

	}

}

/**
 * Add our example form
 *
 * @uses "caldera_forms_get_form_templates"
 *
 * @since 0.1.0
 *
 * @param array $forms Example forms.
 *
 * @return array
 */
function cf_braintree_example_form( $forms ) {
	$forms['braintree-single'] = array(
		'name'     => __( 'BrainTree One Time Payment Form Example', 'cf-braintree' ),
		'template' => include CF_BRAINTREE_PATH . 'includes/templates/braintree-example.php'
	);

	$forms['braintree-plan'] = array(
		'name'     => __( 'BrainTree Payment Plan Example', 'cf-braintree' ),
		'template' => include CF_BRAINTREE_PATH . 'includes/templates/braintree-example-plan.php'
	);

	return $forms;

}

/**
 * Prepare meta to save with entry
 *
 * @since 0.1.0
 *
 * @param object $result
 *
 * @return array
 */
function cf_braintree_prepare_meta( $result ) {
	$meta= array();

	if ( is_object( $result ) ) {

		$metas = array(
			'orderID',
			'amount',
			'planID',
			'status',
			'type',
			'merchantAccountId',
			'processorAuthorizationCode'

		);


		foreach( $metas as $key ) {
			if ( array_key_exists( $key, $result->transaction->_attributes) ) {
				$meta[ $key ] = $result->transaction->__get( $key );
			}


		}

		$meta[ 'time' ] = time();

	}

	return $meta;

}


function cf_braintree_fields(){
	return array(
		array(
			'id'       => 'sandbox',
			'label'    => __( 'Test Mode', 'cf-braintree' ),
			'type'     => 'checkbox',
			'required' => false,
		),
		array(
			'id'    => 'braintree_merchant_id',
			'label' => __( 'Merchant ID', 'cf-braintree' ),
			'desc'  => __( 'Enter your unique merchant ID (found on the portal under API Keys when you first login).', 'cf-braintree' ),
			'type'  => 'text',
			'magic' => false,
		),
		array(
			'id'    => 'braintree_merchant_account_id',
			'label' => __( 'Merchant Account ID', 'cf-braintree' ),
			'desc'  => __( 'Enter your unique merchant account ID (found under Settings > Processing > Merchant Accounts).', 'cf-braintree' ),
			'type'  => 'text',
			'magic' => false,
		),
		array(
			'id'    => 'braintree_public_key',
			'label' => __( 'Public Key', 'cf-braintree' ),
			'desc'  => __( 'Enter your public key (found on the portal under API Keys when you first login).', 'cf-braintree' ),
			'type'  => 'text',
			'magic' => false,
		),
		array(
			'id'    => 'braintree_private_key',
			'label' => __( 'Private Key', 'cf-braintree' ),
			'desc'  => __( 'Enter your private key (found on the portal under API Keys when you first login).', 'cf-braintree' ),
			'type'  => 'text',
			'magic' => false,
		),
		array(
			'id'       => 'orderID',
			'label'    => __( 'Order ID', 'cf-braintree' ),
			'required' => false,
		),
		array(
			'id'       => 'submitForSettlement',
			'label'    => __( 'Submit For Settlement Immediately?', 'cf-braintree' ),
			'required' => false,
			'type'     => 'checkbox'
		),
		array(
			'id'       => 'cf-braintree-planOrSingle',
			'label'    => __( 'One time payment or subscription?', 'cf-braintree' ),
			'required' => false,
			'type'     => 'dropdown',
			'options' => array(
				'charge' => __( 'One Time Payment', 'cf-braintree' ),
				'plan' => __( 'Subscription Plan', 'cf-braintree' ),
			)
		),
		array(
			'id'    => 'amount',
			'label' => __( 'Price', 'cf-braintree' ),
		),
		array(
			'id'    => 'cf-braintree-plan-select',
			'label' => __( 'Subscription Plan', 'cf-braintree' ),
			'type' => 'dropdown',
			'magic' => false,
			'options' => array(
				'' => __( '-- Choose Plan --', 'cf-braintree' )
			)
		),
		array(
			'id'    => 'cf-braintree-plan-actual',
			'type' => 'hidden',
			'label' => 'Actual Plan'
		),
		array(
			'id'    => 'cardholderName',
			'label' => __( 'Cardholder Name', 'cf-braintree' ),
		),
		array(
			'id'    => 'card_number',
			'label' => __( 'Card Number', 'cf-braintree' ),
		),
		array(
			'id'    => 'card_exp_month',
			'label' => __( 'Expiration Month', 'cf-braintree' ),
		),
		array(
			'id'    => 'card_exp_year',
			'label' => __( 'Expiration Year', 'cf-braintree' ),
		),
		array(
			'id'    => 'card_cvc',
			'label' => __( 'CVV Code', 'cf-braintree' ),
		),
		array(
			'id'    => 'first_name',
			'label' => __( 'Customer First Name', 'cf-braintree' ),
		),
		array(
			'id'    => 'last_name',
			'label' => __( 'Customer Last Name', 'cf-braintree' ),
		),
		array(
			'id'          => 'customer_email',
			'label'       => __( 'Customer Email', 'cf-braintree' ),
			'allow_types' => 'email',
			'exclude'     => 'system'
		),
		array(
			'id'       => 'card_address',
			'label'    => __( 'Street Address Line 1', 'cf-braintree' ),
			'required' => false,
		),
		array(
			'id'       => 'card_address_2',
			'label'    => __( 'Street Address Line 2', 'cf-braintree' ),
			'required' => false,
		),
		array(
			'id'       => 'card_city',
			'label'    => __( 'City/ Locality', 'cf-braintree' ),
			'required' => false,
		),
		array(
			'id'       => 'card_state',
			'label'    => __( 'State/ Providence/ Region', 'cf-braintree' ),
			'required' => false,
		),
		array(
			'id'       => 'card_zip',
			'label'    => __( 'Zip Code/ Postal Code', 'cf-braintree' ),
			'required' => false,
		),
		array(
			'id'       => 'card_country',
			'label'    => __( 'Country Code', 'cf-braintree' ),
			'required' => true,
		),
	);
	
}

function cf_braintree_get_plans( $merchant_id, $public_key, $private_key, $sandbox = false ){
	if ( $sandbox ) {
		Braintree_Configuration::environment( 'sandbox' );
	} else {
		Braintree_Configuration::environment( 'production' );
	}

	Braintree_Configuration::merchantId( $merchant_id );
	Braintree_Configuration::publicKey( $public_key );
	Braintree_Configuration::privateKey( $private_key );

	$the_plans = array();
	$plans = Braintree_Plan::all();
	if( ! empty( $plans ) ){
		/** @var Braintree_Plan $plan */
		foreach ( $plans as $plan ){
			$the_plans[ $plan->id ] = $plan->name;
		}
	}

	return $the_plans;

}

function cf_braintree_get_plans_ajax_cb(){
	$fields = array(
		'plans-nonce',
		'public-key',
		'private-key',
		'merchant-id',
		'sandbox'
	);
	
	$values = array();
	$prefix = 'cf-braintree-';
	foreach( $fields as $field ){
		if( ! isset( $_GET[ $prefix . $field ] ) ){
			status_header( 400 );
			exit;
		}
		if( 'sandbox' == $field ){
			if( 'on' == $_GET[ $prefix . $field ] ){
				$values[ $field ] = true;
			}else{
				$values[ $field ] = false;
			}
		}elseif ( 'plans-nonce' == $field ){
			continue;
		}else{
			$values[ $field ] = strip_tags( $_GET[ $prefix . $field ] );
		}
		
	}
	
	if( ! current_user_can( Caldera_Forms::get_manage_cap( 'admin' ) ) || ! wp_verify_nonce( $_GET[ $prefix . 'plans-nonce' ], 'cf-braintree-plans' )  ) {
		status_header( 403 );
		exit;
	}
	
	wp_send_json_success( cf_braintree_get_plans( $values[  'merchant-id' ], $values[ 'public-key' ], $values[ 'private-key' ], $values[ 'sandbox' ] ) );
}

