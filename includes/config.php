<?php
/**
 * Processor config UI for BrainTree for Caldera Forms
 *
 * @package   cf_braintree
 * @author    Josh Pollock for CalderaWP LLC (email : Josh@CalderaWP.com)
 * @license   GPL-2.0+
 * @link
 * @copyright 2015 Josh Pollock for CalderaWP LLC
 */

if ( class_exists( 'Caldera_Forms_Processor_UI' ) ) {
	echo Caldera_Forms_Processor_UI::ssl_notice( 'Braintree for Caldera Forms' );
	$cf_braintree_config_fields = cf_braintree_fields();
	$config_fields              = Caldera_Forms_Processor_UI::config_fields( $cf_braintree_config_fields );
	echo $config_fields;
	echo '<input type="hidden"  id="cf-braintree-plans" name="{{_name}}[cf_braintree_plans]" class="field-config" value="{{json cf_braintree_plans }}">';
	?>
	<script type="text/javascript">
		jQuery(document).ready(function( $ ) {

			var $chooser = $( '#cf-braintree-planOrSingle' );
			var $amount = $( '#amount' );
			var $plan = $( '#cf-braintree-plan-select' );
			var $planButton = $( '#cf-braintree-get-plans' );
			var $spinner = $( '#cf-braintree-plans-spinner' );
			var $merchantId = $( '#braintree_merchant_id' );
			var $publicKey = $( '#braintree_public_key' );
			var $privateKey = $( '#braintree_private_key' );
			var $sandbox = $( '#sandbox' );
			var $planHidden = $( '#cf-braintree-plan-actual' );
			var $storePlans = $( '#cf-braintree-plans' );

			$chooser.change( function(){
				hideShow();
			});

			if( $storePlans.val() ){
				setUpPlans( $storePlans.val() );
			}

			if( $planHidden.val() ){
				$plan.val( $planHidden.val() );
			}

			$plan.change( function(){
				$planHidden.val( $plan.val() );
			});

			hideShow();
			function hideShow() {
				if( 'plan' == $chooser.val() ) {
					hide( $amount );
					show( $plan );
				}else{
					hide( $plan );
					show( $amount );
				}
			}
			
			$planButton.on( 'click', function(e){
				e.preventDefault();
				getPlans();
			});
			
			function getPlans() {
				$spinner.attr( 'aria-hidden', false ).css( 'visibility', 'visible' ).show();
				var data = {
					'cf-braintree-plans-nonce': '<?php echo wp_create_nonce( 'cf-braintree-plans' ); ?>',
					'cf-braintree-public-key' : $publicKey. val(),
					'cf-braintree-private-key' : $privateKey.val(),
					'cf-braintree-merchant-id' : $merchantId.val(),
					'cf-braintree-sandbox' : $sandbox.val(),
					action: 'cf_braintree_get_plans'
				};
				$.get( ajaxurl, data ).done( function( r ){
					$storePlans.val( JSON.stringify( r.data ) );
					setUpPlans( r.data );
				}).always( function(){
					$spinner.attr( 'aria-hidden', true ).css( 'visibility', 'hidden' ).hide();
				})
			}

			function hide( $el ){
				$( '#' + $el.attr( 'id'  ) + '-wrap' ).attr( 'aria-hidden', true ).css( 'visibility', 'hidden' ).hide();

			}

			function show( $el ){
				$( '#' + $el.attr( 'id'  ) + '-wrap' ).attr( 'aria-hidden', false ).css( 'visibility', 'visible' ).show();

			}

			function setUpPlans( plans ){
				if( 'string' == typeof  plans ){
					plans = JSON.parse( plans );
				}
				$plan.empty();

				$.each( plans , function(value,key) {
					$plan.append( $( '<option></option>' ).attr( 'value', value ).text( key ) );
				});
			}

		});
	</script><?php
}
