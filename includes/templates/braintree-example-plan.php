<?php
return array(
	'_last_updated' => 'Tue, 31 Jan 2017 18:34:55 +0000',
	'ID' => 'CF1234567',
	'cf_version' => '1.5.0-b-2',
	'name' => 'Credit Card Payment',
	'scroll_top' => 0,
	'description' => '														',
	'success' => 'Form has been successfully submitted. Thank you.						',
	'db_support' => 1,
	'pinned' => 0,
	'hide_form' => 1,
	'check_honey' => 1,
	'avatar_field' => '',
	'form_ajax' => 1,
	'custom_callback' => '',
	'layout_grid' =>
		array(
			'fields' =>
				array(
					'fld_7028197' => '1:1',
					'fld_8907160' => '1:1',
					'fld_460927' => '1:2',
					'fld_5959061' => '1:2',
					'fld_6500653' => '2:1',
					'fld_9592358' => '2:1',
					'fld_8459878' => '3:1',
					'fld_9549680' => '3:2',
					'fld_528232' => '3:3',
					'fld_8482198' => '4:1',
					'fld_9046738' => '5:1',
					'fld_2664339' => '5:2',
					'fld_859490' => '5:3',
					'fld_3750734' => '6:1',
					'fld_8099154' => '6:2',
					'fld_8563027' => '7:1',
				),
			'structure' => '6:6|12|6:4:2|12|4:4:4|6:6|12',
		),
	'fields' =>
		array(
			'fld_7028197' =>
				array(
					'ID' => 'fld_7028197',
					'type' => 'text',
					'label' => 'First Name',
					'slug' => 'first_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '{user:first_name}',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_8907160' =>
				array(
					'ID' => 'fld_8907160',
					'type' => 'text',
					'label' => 'Last Name',
					'slug' => 'last_name',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '{user:last_name}',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_460927' =>
				array(
					'ID' => 'fld_460927',
					'type' => 'email',
					'label' => 'Email',
					'slug' => 'email',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '{user:user_email}',
						),
				),
			'fld_5959061' =>
				array(
					'ID' => 'fld_5959061',
					'type' => 'text',
					'label' => 'Name On Card',
					'slug' => 'name_on_card',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '%first_name% %last_name%',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_6500653' =>
				array(
					'ID' => 'fld_6500653',
					'type' => 'text',
					'label' => 'Address Line 1',
					'slug' => 'address_line_1',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_9592358' =>
				array(
					'ID' => 'fld_9592358',
					'type' => 'text',
					'label' => 'Address Line 2',
					'slug' => 'address_line_2',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_8459878' =>
				array(
					'ID' => 'fld_8459878',
					'type' => 'text',
					'label' => 'City',
					'slug' => 'city',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_9549680' =>
				array(
					'ID' => 'fld_9549680',
					'type' => 'text',
					'label' => 'Zip Code',
					'slug' => 'zip_code',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_528232' =>
				array(
					'ID' => 'fld_528232',
					'type' => 'states',
					'label' => 'State/ Province',
					'slug' => 'state_province',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
						),
				),
			'fld_8482198' =>
				array(
					'ID' => 'fld_8482198',
					'type' => 'text',
					'label' => 'Card Number',
					'slug' => 'card_number',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_9046738' =>
				array(
					'ID' => 'fld_9046738',
					'type' => 'text',
					'label' => 'Expiration Month',
					'slug' => 'expiration_month',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'masked' => 1,
							'mask' => 99,
						),
				),
			'fld_2664339' =>
				array(
					'ID' => 'fld_2664339',
					'type' => 'text',
					'label' => 'Expiration Year',
					'slug' => 'expiration_year',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'masked' => 1,
							'mask' => 99,
						),
				),
			'fld_859490' =>
				array(
					'ID' => 'fld_859490',
					'type' => 'text',
					'label' => 'CVV',
					'slug' => 'cvv',
					'conditions' =>
						array(
							'type' => '',
						),
					'required' => 1,
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'placeholder' => '',
							'default' => '',
							'type_override' => 'text',
							'mask' => '',
						),
				),
			'fld_3750734' =>
				array(
					'ID' => 'fld_3750734',
					'type' => 'button',
					'label' => 'Reset',
					'slug' => 'reset',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'type' => 'reset',
							'class' => 'btn btn-default',
							'target' => '',
						),
				),
			'fld_8099154' =>
				array(
					'ID' => 'fld_8099154',
					'type' => 'button',
					'label' => 'Pay',
					'slug' => 'pay',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'type' => 'submit',
							'class' => 'btn btn-default',
							'target' => '',
						),
				),
			'fld_8563027' =>
				array(
					'ID' => 'fld_8563027',
					'type' => 'hidden',
					'label' => 'order_id',
					'slug' => 'order_id',
					'conditions' =>
						array(
							'type' => '',
						),
					'caption' => '',
					'config' =>
						array(
							'custom_class' => '',
							'default' => '',
						),
				),
		),
	'page_names' =>
		array(
			0 => 'Page 1',
		),
	'mailer' =>
		array(
			'on_insert' => 1,
			'sender_name' => 'Caldera Forms Notification',
			'sender_email' => 'admin@example.com',
			'reply_to' => '',
			'email_type' => 'html',
			'recipients' => '',
			'bcc_to' => '',
			'email_subject' => 'BrainSingle',
			'email_message' => '{summary}',
		),
	'processors' =>
		array(
			'fp_41257708' =>
				array(
					'ID' => 'fp_41257708',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'increment_capture',
					'config' =>
						array(
							'start' => 42,
							'field' => 'fld_8563027',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
			'fp_43339419' =>
				array(
					'ID' => 'fp_43339419',
					'runtimes' =>
						array(
							'insert' => 1,
						),
					'type' => 'cf_braintree',
					'config' =>
						array(
							'sandbox' => 'on',
							'braintree_merchant_id' => 'CHANGE ME!',
							'braintree_merchant_account_id' => 'CHANGE ME!',
							'braintree_public_key' => 'CHANGE ME!',
							'braintree_private_key' => 'CHANGE ME!',
							'orderID' => '',
							'cf-braintree-planOrSingle' => 'plan',
							'amount' => 99,
							'cf-braintree-plan-select' => NULL,
							'cf-braintree-plan-actual' => '',
							'cardholderName' => '%name_on_card%',
							'card_number' => '%card_number%',
							'card_exp_month' => '%expiration_month%',
							'card_exp_year' => '%expiration_year%',
							'card_cvc' => '%cvv%',
							'first_name' => '%first_name%',
							'last_name' => '%last_name%',
							'customer_email' => 'fld_460927',
							'_required_bounds' =>
								array(
									0 => 'customer_email',
								),
							'card_address' => '%address_line_1%',
							'card_address_2' => '%address_line_2%',
							'card_city' => '%city%',
							'card_state' => '%state_province%',
							'card_zip' => '%zip_code%',
							'card_country' => 'US',
							'cf_braintree_plans' => '""',
						),
					'conditions' =>
						array(
							'type' => '',
						),
				),
		),
	'conditional_groups' =>
		array(
			'_open_condition' => '',
		),
	'settings' =>
		array(
			'responsive' =>
				array(
					'break_point' => 'sm',
				),
		),
	'version' => '1.5.0-b-2',
);