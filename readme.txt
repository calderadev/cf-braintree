=== BrainTree for Caldera Forms ===
Contributors:      Shelob9, Desertsnowman
Donate link:       https://CalderaWP.com/downloads/braintree-for-caldera-forms
Tags:              calderawp, caldera forms, wpform, form, responsive
Requires at least: 4.0
Tested up to:      4.3
Stable tag: 1.2.4
License:           GPLv2 or later
License URI:       http://www.gnu.org/licenses/gpl-2.0.html

BrainTree for Caldera Forms

== Description ==



== Installation ==

= Manual Installation =

1. Upload the entire `/braintree-for-caldera-forms` directory to the `/wp-content/plugins/` directory.
2. Activate BrainTree for Caldera Forms through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.0.0 =
* First release

= 1.1.0 =
* Add UI option to submit for settlement immediately.

== Upgrade Notice ==

= 0.1.0 =
First Release
